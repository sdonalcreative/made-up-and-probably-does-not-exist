/*jslint browser:true */
/*global AudioContext */

var timeContainer = document.querySelector('body > nav.taskbar > time');
var taskbahButton = document.querySelector('[data-action="lock-the-taskbah"]');
var taskbah = document.querySelector('body > nav.taskbar');
var taskbahContextMenu = document.querySelector('body > nav.taskbar-context');
var taskbahContextMenuPosition = { x: 0, y: 0 };

function prePad(string, length, character) {
    if (typeof string !== 'string') {
        string = string + '';
    }

    if (string.length >= length) { return string; }

    return (Array(length - string.length + 1).join(character)) + string;
}
function ISODateString(date){
    return date.getUTCFullYear() + '-' +
        prePad(date.getUTCMonth()+1, 2, '0') + '-' +
        prePad(date.getUTCDate(), 2, '0') + 'T' +
        prePad(date.getUTCHours(), 2, '0') + ':' +
        prePad(date.getUTCMinutes(), 2, '0') + ':' +
        prePad(date.getUTCSeconds(), 2, '0') + 'Z';
}

var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
function updateTime() {
    var dateNow = new Date();

    timeContainer.innerHTML =
        prePad(dateNow.getHours(), 2, '0') + ':' + prePad(dateNow.getMinutes(), 2, '0') + '\n' +
        prePad(dateNow.getDate(), 2, '0') + '/' + prePad(dateNow.getMonth() + 1, 2, '0') + '/' + dateNow.getFullYear();
    timeContainer.setAttribute('datetime', ISODateString(dateNow));
    timeContainer.setAttribute('title', prePad(dateNow.getDate(), 2, '0') + ' ' + monthNames[dateNow.getMonth()] + ' ' + dateNow.getFullYear() + '\n' + dayNames[dateNow.getDay()]);
}
updateTime();
setInterval(updateTime, 1000);

function prepareAudioContext() {
    var url = 'sound/taskbah.mp3';

    var context = new AudioContext();

    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';
    request.onload = function() {
        context.decodeAudioData(
            request.response,
            function(response) {
                context.buffer = response;
            },
            function () { console.error('Failed to decode audio!'); }
        );
    };
    request.send();

    return context;
}

function prepareBasicAudio() {
    return document.querySelector('audio');
}

var setAudioState;
var audioData;
var audioSource;
if (window.AudioContext !== undefined) {
    audioData = prepareAudioContext();

    setAudioState = function (enabled) {
        if (audioData.buffer !== null) {
            if (enabled) {
                audioSource = audioData.createBufferSource();
                audioSource.connect(audioData.destination);
                audioSource.buffer = audioData.buffer;
                audioSource.loop = true;
                audioSource.start();
            } else {
                audioSource.stop();
            }
        }
    };
} else {
    audioData = prepareBasicAudio();

    setAudioState = function (enabled) {
        if (enabled) {
            audioData.play();
        } else {
            audioData.pause();
            audioData.currentTime = 0;
        }
    };
}

function sendGAEvent(taskbarLocked) {
    if (!window.ga) { return; }

    window.ga('send', 'event', 'Taskbar', taskbarLocked ? 'Locked' : 'Unlocked');
}

function showTaskbahModal(x, y) {
    taskbahContextMenu.style.left = null;
    taskbahContextMenu.style.right = null;
    taskbahContextMenu.style.bottom = null;

    if (x > document.body.clientWidth - taskbahContextMenu.clientWidth) {
        taskbahContextMenu.style.right = 0;
    } else {
        taskbahContextMenu.style.left = x + 'px';
    }
    taskbahContextMenu.style.bottom = document.body.clientHeight - y + 'px';


    if (x !== taskbahContextMenuPosition.x || y !== taskbahContextMenuPosition.y) {
        // Force a reflow, to restart the animation.
        taskbahContextMenu.classList.remove('active');
        taskbahContextMenu.offsetWidth = taskbahContextMenu.offsetWidth;
    }
    taskbahContextMenu.classList.add('active');

    taskbahContextMenuPosition.x = x;
    taskbahContextMenuPosition.y = y;
}
function hideTaskbahModal() {
    taskbahContextMenu.classList.remove('active');
}

function handleDocumentClick(event) {
    if (event.target === taskbah && event.button === 2) {
        showTaskbahModal(event.pageX, event.pageY);
        return;
    }

    if (taskbahContextMenu.classList.contains('active')) {
        hideTaskbahModal();

        event.preventDefault();
        event.stopPropagation();
    }
}
function handleTaskbahClick(event) {
    event.preventDefault();

    if (event.target !== this) {
        event.stopPropagation();
        return;
    }

    if (!taskbahContextMenu.classList.contains('active')) {
        showTaskbahModal(event.pageX, event.pageY);

        event.stopPropagation();
    }
}
function handleTaskbahButtonClick(event) {
    event.preventDefault();

    if (taskbahButton.hasAttribute('checked')) {
        taskbahButton.removeAttribute('checked');
        taskbah.removeAttribute('data-locked');

        sendGAEvent(false);
        setAudioState(false);

        event.stopPropagation();
    } else {
        taskbahButton.setAttribute('checked', '');
        taskbah.setAttribute('data-locked', '');

        sendGAEvent(true);
        setAudioState(true);

        event.stopPropagation();
    }
}

document.addEventListener('click', handleDocumentClick);
document.addEventListener('contextmenu', handleDocumentClick);

taskbah.addEventListener('click', handleTaskbahClick);
taskbah.addEventListener('contextmenu', handleTaskbahClick);

taskbahButton.addEventListener('click', handleTaskbahButtonClick);
taskbahButton.addEventListener('contextmenu', handleTaskbahButtonClick);
