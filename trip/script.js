/*jslint browser: true */
/*global AudioContext, WebAudioScheduler */

var audioContext = new AudioContext();
var audioSchedule = new WebAudioScheduler({ context: audioContext });

var globalOffset = -0.005;
var itemsToLoad = 4;

var loopBuffer;
var loopStartTime;
var loopNumber = 0;
var loopTimeline;

var introBuffer;
var introTimeline;

var buttonTogglePlayState = document.querySelector('[data-action~=toggle-play-state]');

var naughtyHouse = document.querySelector('#naughty-house');

var calvin = document.querySelector('#calvin');
var calvinBody = calvin.querySelector('.body');
var calvinAnimations = {
    squeal: 'rotate(-40deg)',
    squee: 'rotate(-20deg)',
    toot: 'rotate(30deg)'
};

var klein = document.querySelector('#klein');
var kleinBody = klein.querySelector('.body');
var kleinAnimations = {
    bap: 'translateY(5%)',
    boop: 'rotate(-30deg)'
};

var lewdAnimations = {
    ahh: 750,
    uh: 600,
    ohh: 350,
    oh: 250,
    ah: 250,
    ha: 250,
    argh: 700
};

function loadAudio(url, audioContext, callback) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';
    request.onload = function() {
        audioContext.decodeAudioData(
            request.response,
            function(audioBuffer) {
                callback(audioBuffer);
            },
            function () { console.error('Failed to decode audio!'); }
        );
    };
    request.send();
}

function loadTimeline(url, callback) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'json';
    request.onload = function() {
        callback(request.response);
    };
    request.send();
}

function createAudioSource(audioBuffer, audioContext) {
    var audioSource = audioContext.createBufferSource();
    audioSource.connect(audioContext.destination);
    audioSource.buffer = audioBuffer;

    return audioSource;
}

function startAnimation(ev) {
    var currentTime = ev.playbackTime;

    audioSchedule.insert(currentTime, startIntro);
}

function startIntro(ev) {
    var currentTime = ev.playbackTime;

    audioSchedule.insert(currentTime + introBuffer.duration, startLoop);

    createAudioSource(introBuffer, audioContext).start();
    scheduleIntro(ev);
}
function scheduleIntro(ev) {
    var currentTime = ev.playbackTime;

    introTimeline.forEach(function (action) {
        audioSchedule.insert(currentTime + action.start + globalOffset, animationEvent, { action: action, type: "start" });
        audioSchedule.insert(currentTime + action.end + globalOffset, animationEvent, { action: action, type: "end" });
    });
}

function startLoop(ev) {
    var loop = createAudioSource(loopBuffer, audioContext);

    loop.loop = true;
    loop.start();

    loopStartTime = ev.playbackTime;

    scheduleLoop(ev);
}
function scheduleLoop(ev) {
    var currentTime = loopStartTime + loopBuffer.duration * loopNumber;

    loopTimeline.forEach(function (action) {
        audioSchedule.insert(currentTime + action.start + globalOffset, animationEvent, { action: action, type: "start" });
        audioSchedule.insert(currentTime + action.end + globalOffset, animationEvent, { action: action, type: "end" });
    });

    audioSchedule.insert(currentTime + loopBuffer.duration, scheduleLoop);

    loopNumber += 1;
}

function animationEvent(ev) {
    var action = ev.args.action,
        type = ev.args.type;

    if (type === 'end') {
        if (calvinAnimations[action.name]) {
            calvinBody.style.transform = 'none';
        }

        if (kleinAnimations[action.name]) {
            kleinBody.style.transform = 'none';
        }

        return;
    }

    if (calvinAnimations[action.name]) {
        calvinBody.style.transform = calvinAnimations[action.name];
    }

    if (kleinAnimations[action.name]) {
        kleinBody.style.transform = kleinAnimations[action.name];
    }

    if (lewdAnimations[action.name]) {
        spawnLewdText(action.name, lewdAnimations[action.name]);
    }
}

function spawnLewdText(text, duration) {
    var exclamationElement = document.createElement('figure'),
        textElement = document.createElement('p');

    exclamationElement.setAttribute('class', 'exclamation');
    exclamationElement.style.transform = 'rotate(' + (Math.random() * -15 - 5) + 'deg)';

    textElement.textContent = text;
    textElement.style.animationDuration = (duration / 1000) + 's';

    textElement.addEventListener('animationend', function () {
        exclamationElement.parentElement.removeChild(exclamationElement);
    });

    exclamationElement.insertBefore(textElement, null);
    naughtyHouse.parentElement.insertBefore(exclamationElement, naughtyHouse);
}

function setPlayState(isPlaying) {
    if (!isPlaying && audioContext.state !== 'suspended') {
        audioContext.suspend();
        return true;
    }

    if (isPlaying && audioContext.state !== 'running') {
        audioContext.resume();
        return true;
    }
}
function getIsPlaying() {
    return audioContext.state === 'running';
}

function playStateChanged() {
    buttonTogglePlayState.setAttribute('data-state', getIsPlaying() ? 'playing' : 'paused');
}

function beginIfLoaded() {
    itemsToLoad -= 1;

    if (itemsToLoad === 0) {
        audioSchedule.start(startAnimation);
    }
}

loadAudio('sounds/intro.mp3', audioContext, function (audioBuffer) { introBuffer = audioBuffer; beginIfLoaded(); });
loadTimeline('sounds/intro-timeline.json', function (timeline) { introTimeline = timeline; beginIfLoaded(); });

loadAudio('sounds/loop.mp3', audioContext, function (audioBuffer) { loopBuffer = audioBuffer; beginIfLoaded(); });
loadTimeline('sounds/loop-timeline.json', function (timeline) { loopTimeline = timeline; beginIfLoaded(); });

function bind() {
    buttonTogglePlayState.addEventListener('click', function () { setPlayState(!getIsPlaying()); });

    audioContext.addEventListener('statechange', playStateChanged);
}

window.addEventListener('load', function () {
    bind();
});
