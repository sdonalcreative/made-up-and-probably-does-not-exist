/*jslint node: true */

var fs = require('fs');

var labelsFile = process.argv[2];

fs.readFile(labelsFile, 'utf8', (err, data) => {
    if (err) throw err;

    var labels = [];

    data.split('\n').forEach((labelLine) => {
        var label = {}

        if (!labelLine.trim()) { return; }

        labelLine.split('\t').forEach((field, index) => {
            if (index === 0) {
                label.start = parseFloat(field);
            }
            if (index === 1) {
                label.end = parseFloat(field);
            }
            if (index === 2) {
                label.name = field.trim();
            }
        });

        labels.push(label);
    });

    console.log(JSON.stringify(labels));
});
