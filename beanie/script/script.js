function replaceAt(str, index, character) {
    return str.substr(0, index) + character + str.substr(index + character.length);
}

function matchCase(a, b) {
    if (a === a.toLowerCase()) {
        return b.toLowerCase();
    } else {
        return b.toUpperCase();
    }
}


function delayedCaps(str) {
    var search = /\b[A-Z]+(\s[A-Z]+)?/g;

    return str.replace(search, function (word) {
        return word.charAt(0).toLowerCase() + word.slice(1).toUpperCase();
    });
}

function fatFingers(str) {
    var keyboardLines = [
            '11!1',
            '//?/',
            'qwertyuiop[]',
            'asdfghjkl;\'',
            '\\zxcvbnm,./',
            'яшертыуиопющэ',
            'асдфгчйкльж',
            'зхцвбнм;:',
            'llñ',
        ],
        search = /[^\s]+/g,
        likelihood = 1.2;

    return str.replace(search, function (word) {
        var i, o,
            character, line,
            replaceLine, replaceIndex, replaceCharacter;

        for (i = 0; i < word.length; i += 1) {
            character = word[i];

            replaceLine = '';
            replaceIndex = -1;

            for (o = 0; o < keyboardLines.length; o += 1) {
                line = keyboardLines[o];

                replaceIndex = line.indexOf(character.toLowerCase());
                if (replaceIndex >= 0) {
                    replaceLine = line;
                    break;
                }
            }

            if (replaceIndex >= 0) {
                replaceCharacter =
                    line[Math.max(0, Math.min(line.length - 1,
                        replaceIndex + Math.round((Math.random() - 0.5) * likelihood)
                    ))];

                word = replaceAt(
                    word,
                    i,
                    matchCase(character, replaceCharacter)
                );
            }
        }

        return word;
    });
}

function sonicSpace(str) {
    var search = /\w\s\w/g,
        likelihood = 0.3;

    return str.replace(search, function (word) {
        if (Math.random() < likelihood) {
            if (Math.random() > 0.5) {
                return word[0] + word[2] + ' ';
            } else {
                return ' ' + word[0] + word[2];
            }
        } else { return word; }
    });
}

function beanify(str) {
    str = delayedCaps(str);
    str = fatFingers(str);
    str = sonicSpace(str);

    return str;
}

function bind() {
    var input = document.querySelector('textarea'),
        resultContainer = document.querySelector('p');

    if (!input || !resultContainer) { return; }

    function updateResult() {
        resultContainer.textContent = beanify(input.value);
    }

    input.addEventListener('input', updateResult);

    updateResult();
}

bind();
