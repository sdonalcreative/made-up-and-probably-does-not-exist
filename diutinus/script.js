'use strict';

var presentCanvas = document.querySelector('canvas');

var bufferCanvas = document.createElement('canvas');
bufferCanvas.width = presentCanvas.width;
bufferCanvas.height = presentCanvas.height;

var audioContext = new AudioContext();

var audio = new Audio();
var audioSource = audioContext.createMediaElementSource(audio);
audio.loop = true;

function Analyser(audioContext, audioInput) {
    var self = this,
        i;

    self.audioContext = audioContext;
    self.audioInput = audioInput;

    self.numChannels = 2; // Not safe to change at this time
    self.spectrumSize = 512;
    self.maxFrequencyBin = self.spectrumSize / 8;
    self.fftLength = self.spectrumSize / 2;
    self.freqLength = self.fftLength;

    self.audioSplitter = self.audioContext.createChannelSplitter(self.numChannels);
    self.audioOutput = self.audioContext.createChannelMerger(self.numChannels);

    self.audioInput.connect(self.audioSplitter);

    self.channelAnalyser = [];
    self.channel = [];
    self.channelMax = [];

    self.filtered = new Float32Array(self.fftLength * self.numChannels);
    self.filteredLength = self.filtered.length;

    for (i = 0; i < self.numChannels; i += 1) {
        self.channelAnalyser[i] = self.audioContext.createAnalyser();
        self.channelAnalyser[i].fftSize = self.spectrumSize;
        self.channelAnalyser[i].smoothingTimeConstant = 0;
        self.audioSplitter.connect(self.channelAnalyser[i], i, 0);
        self.channelAnalyser[i].connect(self.audioOutput, 0, i);

        self.channel[i] = new Float32Array(self.fftLength);
        self.channelMax[i] = new Float32Array(self.fftLength);
    }

    self.analyserMin = self.channelAnalyser[0].minDecibels;
    self.analyserMax = self.channelAnalyser[0].maxDecibels;
    self.analyserRange = self.analyserMax - self.analyserMin;
    self.analyserLoudThreshold = self.analyserMin + self.analyserRange / 3;

    self.intensity = 0;
    self.unboundIntensity = 0;
    self.fadingIntensity = 0;

    self.reset();
}

Analyser.prototype.reset = function () {
    var self = this,
        i;

    for (i = 0; i < self.numChannels; i += 1) {
        self.channel[i].fill(0);
        self.channelMax[i].fill(0.0001);
    }

    self.maxFrequencyBin = self.spectrumSize / 8;
    self.maxVisibleFrequencyBin = self.maxFrequencyBin;
    self.resize(self.maxVisibleFrequencyBin);

    self.filtered.fill(0);
};


/**
 * Adjusts the upper clipping bin of the analyser, whilst keeping all current data.
 * @param {number} maxFrequencyBin The upper limit to display on the analyser.
 */
Analyser.prototype.resize = function (maxFrequencyBin) {
    var self = this,
        originalFiltered = self.filtered;

    self.freqLength = self.fftLength - (self.fftLength - maxFrequencyBin);
    self.filteredLength = self.freqLength * self.numChannels;
};

/**
 * Populates the channel[][] arrays with FFT data from each of the channelAnalysers,
 * cleaning it up in the process.
 */
Analyser.prototype.updateRawFFT = function () {
    var self = this,
        i,
        o,
        localMaxFrequencyBin = 0;

    for (i = 0; i < self.numChannels; i += 1) {
        self.channelAnalyser[i].getFloatFrequencyData(self.channel[i]);

        for (o = 0; o < self.fftLength; o += 1) {

            if (self.channel[i][o] > self.analyserLoudThreshold && o > localMaxFrequencyBin) {
                localMaxFrequencyBin = o;
            }

            self.channel[i][o] = Math.max(
                Math.min((self.channel[i][o] - self.analyserMin) / self.analyserRange, 1),
                0
            );
        }
    }

    self.maxFrequencyBin = Math.max(
        64,
        (self.maxFrequencyBin * 99 + localMaxFrequencyBin) / 100,
        localMaxFrequencyBin
    );
};

Analyser.prototype.smoothAndTransform = function (smoothAmount) {
    var self = this,
        channel,
        freq,
        leftRight,
        currentTotal,
        thisFreq,
        clampFreq,
        newIntensity = 0,
        i;

    for (channel = 0; channel < self.numChannels; channel += 1) {
        for (freq = 0; freq < self.freqLength; freq += 1) {
            self.channelMax[channel][freq] -= self.channelMax[channel][freq] / 48;

            self.channelMax[channel][freq] = Math.max(
                self.channelMax[channel][freq],
                self.channel[channel][freq],
                0.0001
            );
        }
    }

    leftRight = Math.floor(smoothAmount / 2);
    for (freq = 0; freq < self.filteredLength; freq += 1) {
        currentTotal = 0;

        for (i = -leftRight; i <= leftRight; i += 1) {
            thisFreq =
                getPointSplitWrap(self.channel[1], self.channel[0], self.freqLength, freq + i) /
                getPointSplitWrap(self.channelMax[1], self.channelMax[0], self.freqLength, freq + i);

            currentTotal += thisFreq;

            if (i === 0) {
                currentTotal += thisFreq; // Add again to give the actual frequency a kick.
            }
        }
        currentTotal /= leftRight * 2 + 2;

        clampFreq = Math.min(1, Math.max(0, self.filtered[freq]));

        self.filtered[freq] +=
            (currentTotal - self.filtered[freq]) /
            (((1 - clampFreq) + 0.25) * 2.5);

        newIntensity += self.filtered[freq];
    }

    newIntensity /= self.filteredLength;
    newIntensity *= 2;
    newIntensity = Math.max(0, newIntensity);

    self.unboundIntensity = newIntensity;
    self.fadingIntensity -= self.fadingIntensity / 32;
    self.fadingIntensity = Math.max(self.fadingIntensity, self.unboundIntensity);
    self.intensity = Math.min(1, newIntensity);


    function getPointSplitWrap(a, b, length, position) {
        if (position > length * 2 - 1) { position -= (position / (length * 2)) * length * 2; }
        if (position < 0) { position += (position / (length * 2) + 1) * length * 2; }

        var sLength = length / 2;

        if (position < length)
        {
            if (position >= sLength) return a[Math.floor((position - sLength) * 2)]; // Even numbers
            if (position < sLength) return a[Math.floor((sLength - 1 - position) * 2 + 1)]; // Odd numbers
        }
        else
        {
            position -= length;
            if (position >= sLength) return b[Math.floor((position - sLength) * 2)]; // Even numbers
            if (position < sLength) return b[Math.floor((sLength - 1 - position) * 2 + 1)]; // Odd numbers
        }
    }
};

Analyser.prototype.update = function () {
    var self = this;

    self.updateRawFFT();

    self.resize(Math.round(self.maxFrequencyBin));

    self.smoothAndTransform(Math.floor(
        Math.min(30, Math.max(6, Math.pow((1 - self.intensity) * 15, 2)))
    ));
};

Analyser.prototype.render = function (ctx) {
    var self = this;

    ctx.save();

    ctx.fillStyle = 'rgba(0,0,0, 0.5)';
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'hsl(' + Math.floor(analyser.fadingIntensity * 300) + ', 100%, 50%)';
    ctx.lineJoin = 'round';
    self.renderChannel(ctx);

    ctx.restore();
};

Analyser.prototype.renderChannel = function (ctx) {
    var self = this,
        i,
        radius = 290,
        currentPos,
        cX = 640,
        cY = 360;

    ctx.beginPath();

    ctx.moveTo(
        cX + Math.sin(0) * self.filtered[0] * radius,
        cY + Math.cos(0) * self.filtered[0] * radius
    );

    for (i = 1; i < self.filteredLength; i += 1) {
        currentPos = i / self.filteredLength * Math.PI * 2;

        ctx.lineTo(
            cX + Math.sin(currentPos) * self.filtered[i] * radius,
            cY + Math.cos(currentPos) * self.filtered[i] * radius
        );
    }

    ctx.lineTo(
        cX + Math.sin(0) * self.filtered[0] * radius,
        cY + Math.cos(0) * self.filtered[0] * radius
    );

    ctx.fill();
    ctx.globalCompositeOperation = 'lighter';
    ctx.stroke();
};

var analyser = new Analyser(audioContext, audioSource);
analyser.audioOutput.connect(audioContext.destination);
window.exposed = analyser;

function drawFFT() {
    var bufferCtx = bufferCanvas.getContext('2d'),
        presentCtx = presentCanvas.getContext('2d'),
        previousScale;

    bufferCtx.fillStyle = 'rgba(0, 0, 0, 1)';
    bufferCtx.fillRect(0, 0, 1280, 720);

    previousScale = 0.99 + Math.pow((1 - analyser.unboundIntensity) / 2.5, 2);

    if (previousScale < 1) {
        bufferCtx.drawImage(presentCanvas, 0, 0);
    }

    bufferCtx.save();
    bufferCtx.globalAlpha = Math.min(analyser.intensity + 0.5, 1);
    bufferCtx.translate(640, 360);
    bufferCtx.scale(previousScale, previousScale);
    bufferCtx.drawImage(presentCanvas, -640, -360);
    bufferCtx.restore();

    analyser.update();
    analyser.render(bufferCtx);


    presentCtx.drawImage(bufferCanvas, 0, 0);

    requestAnimationFrame(drawFFT);
}
requestAnimationFrame(drawFFT);

var supportedTypes = [
    'audio/mp3',
    'audio/mpeg',
    'audio/wav',
    'audio/ogg',
    'video/ogg',
    'audio/flac',
    'audio/x-flac'
];

function isValidAudio(mime) {
    return supportedTypes.indexOf(mime) !== -1;
}
function containsValidAudio(dataTransferItems) {
    var i;

    for (i = 0; i < dataTransferItems.length; i += 1) {
        if (isValidAudio(dataTransferItems[i].type)) {
            return true;
        }
    }

    return false;
}

window.addEventListener('drop', function (evt) {
    evt.preventDefault();

    audio.src = URL.createObjectURL(evt.dataTransfer.files[0]);
    audio.play();
});

window.addEventListener('dragover', function (evt) {
    evt.preventDefault();

    if (!containsValidAudio(evt.dataTransfer.items)) {
        evt.dataTransfer.dropEffect = "none";
    } else {
        evt.dataTransfer.dropEffect = "copy";
        evt.dataTransfer.effectAllowed = "copy";
    }
});

window.addEventListener('dragend', function (evt) {
    evt.preventDefault();
});
